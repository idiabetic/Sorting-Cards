# Sorting Cards

An implementation of the Turing project located [here](http://backend.turing.io/module1/projects/sorting_cards).


## Current Status
* Implements the correct class interactions
* Implements Bubble Sort
* Implements Merge Sort
* Rubocop < 5 errors
* Implements Rake call for testing `rake test`

## Todo
* Clean up naming
* Implement single responsibility
* Implement dry principles
