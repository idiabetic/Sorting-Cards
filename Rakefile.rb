# frozen_string_literal:true

require 'rake/testtask'
require 'rubocop/rake_task'

RuboCop::RakeTask.new(:rubocop) do |t|
  t.options = ['--display-cop-names']
end

Rake::TestTask.new do |t|
  puts ''
  puts '|--------------------------------------------------------------------|'
  puts '|                                                                    |'
  puts '|                                                                    |'
  puts '|                                                                    |'
  puts '|                                   ( Hi, Brian, Megan or Sal,  )    |'
  puts '|                                   ( Welcome to my Rakefile    )    |'
  puts '|                                   ( You can run:              )    |'
  puts '|                                   (  - rake tests             )    |'
  puts '|                                   (  - rake rubocop           )    |'
  puts '|                                               o                    |'
  puts '|                                             o                      |'
  puts '|                                           o                        |'
  puts '|                              ____________                          |'
  puts '|                             |   _     _  |                         |'
  puts '|            __               |   O     O  |            __           |'
  puts '|___________|  |______________|_____  _____|___________|  |__________|'
  puts '|           |__|                   |  |                |__|          |'
  puts '|__________________________________|  |______________________________|'
  puts '|                                  |  |                              |'
  puts '|__________________________________|  |______________________________|'
  puts '|                                  |__|                              |'
  puts '|____________________________________________________________________|'
  puts '|                                                                    |'
  puts '|____________________________________________________________________|'
  t.pattern = 'test/*_test.rb'
  puts ''
end
