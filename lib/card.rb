# frozen_string_literal:true

# Implements the Card Class
class Card
  attr_accessor :value, :suit, :absolute_value
  def initialize(value, suit)
    @value = value
    @suit = suit
    @absolute_value = absolute_value_map(@value) + absolute_value_map(@suit)
  end

  def absolute_value_map(value_or_suit)
    card_map = { '2' => 100, '3' => 200, '4' => 300, '5' => 400, '6' => 500, '7' => 600,
                 '8' => 700, '9' => 800, '10' => 900, 'Jack' => 100, 'Queen' => 1100,
                 'King' => 1200, 'Ace' => 1300, 'Clubs' => 1, 'Diamonds' => 2,
                 'Hearts' => 3, 'Spades' => 4 }
    card_map[value_or_suit]
  end
end
