# frozen_string_literal:true

require_relative '../lib/card'

# Implementation of the Deck class
class Deck
  attr_accessor :card_array
  def initialize(card_array)
    @card_array = card_array
  end

  def sort
    sorted_array = @card_array
    loop do
      @card_changed = false
      array_swap_determinator(sorted_array)
      break unless @card_changed
    end
    sorted_array
  end

  def array_swap_determinator(array)
    (array.length - 1).times do |index|
      if array[index].absolute_value > array[index + 1].absolute_value
        array_position_swap(array, index)
        @card_changed = true
      end
    end
    array
  end

  def merge_sort
    mergesort(@card_array)
  end

  def array_position_swap(array, index)
    array[index], array[index + 1] = array[index + 1], array[index]
  end

  def mergesort(unsorted_array)
    if unsorted_array.length <= 1
      unsorted_array
    else
      mid_length = (unsorted_array.length / 2).floor
      left_array = mergesort(unsorted_array[0..mid_length - 1])
      right_array = mergesort(unsorted_array[mid_length..unsorted_array.length])
      merge(left_array, right_array)
    end
  end

  def merge(left_array, right_array)
    if left_array.empty?
      right_array
    elsif right_array.empty?
      left_array
    elsif left_array.first.absolute_value < right_array.first.absolute_value
      [left_array.first] + merge(left_array[1..left_array.length], right_array)
    else
      [right_array.first] + merge(left_array, right_array[1..right_array.length])
    end
  end
end
