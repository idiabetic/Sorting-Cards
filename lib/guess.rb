# frozen_string_literal:true

require_relative '../lib/card'

# Implementation of the Guess class
class Guess
  attr_accessor :response, :card
  def initialize(response, card)
    @response = response
    @card = card
  end

  def correct?
    @response == "#{card.value} of #{card.suit}"
  end

  def feedback
    correct? ? 'Correct!' : 'Incorrect!'
  end
end
