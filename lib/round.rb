# frozen_string_literal:true

require_relative '../lib/guess'
require_relative '../lib/card'
require_relative '../lib/deck'

# Implementation of the Round class
class Round
  attr_accessor :deck, :guesses, :number_correct
  def initialize(deck)
    @deck = deck
    @guesses = []
    @number_correct = 0
  end

  def current_card
    deck.card_array.first
  end

  def record_guess(guess_hash)
    guess = Guess.new("#{guess_hash[:value]} of #{guess_hash[:suit]}",
                      current_card)
    @guesses << guess
    correct_guess_actions
  end

  def percent_correct
    "#{(@number_correct.to_f / @guesses.length.to_f) * 100}%"
  end

  def correct_guess_actions
    if @guesses.last.correct?
      @deck.card_array.delete(current_card)
      @number_correct += 1
    end
  end
end
