# frozen_string_literal:true

require 'minitest/autorun'
require 'minitest/pride'
require_relative '../lib/card'

# Tests the Card Class
class CardTest < Minitest::Test
  def test_it_instantiates
    card = Card.new('Ace', 'Spades')
    assert_instance_of Card, card
  end

  def test_card_has_value
    card = Card.new('Ace', 'Spades')
    assert_equal 'Ace', card.value
  end

  def test_card_has_suit
    card = Card.new('Ace', 'Spades')
    assert_equal 'Spades', card.suit
  end
end
