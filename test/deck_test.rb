# frozen_string_literal:true

require 'minitest/autorun'
require 'minitest/pride'
require_relative '../lib/guess'
require_relative '../lib/card'
require_relative '../lib/deck'

# Tests the CreditCheck Class
class DeckTest < Minitest::Test
  def setup
    @card = Card.new('2', 'Spades')
    @card1 = Card.new('3', 'Spades')
    @card2 = Card.new('4', 'Spades')
  end

  def test_it_instantiates
    deck = Deck.new([@card, @card1, @card2])
    assert_instance_of Deck, deck
  end

  def test_it_takes_an_array_of_cards
    deck = Deck.new([@card, @card1, @card2])
    assert_equal [@card, @card1, @card2], deck.card_array
  end

  def test_it_bubble_sorts
    deck = Deck.new([@card2, @card, @card1])
    assert_equal [@card, @card1, @card2], deck.sort
  end

  def test_it_merge_sorts
    deck = Deck.new([@card2, @card, @card1])
    assert_equal [@card, @card1, @card2], deck.merge_sort
  end

  def test_it_merge_sorts_suit
    card = Card.new('2', 'Hearts')
    card1 = Card.new('2', 'Spades')
    card2 = Card.new('4', 'Clubs')
    deck = Deck.new([card, card2, card1])
    assert_equal [card, card1, card2], deck.merge_sort
  end

  def test_it_bubble_sorts_with_suit
    card = Card.new('2', 'Spades')
    card1 = Card.new('2', 'Hearts')
    card2 = Card.new('4', 'Clubs')
    deck = Deck.new([card2, card, card1])
    assert_equal [card1, card, card2], deck.sort
  end
end
