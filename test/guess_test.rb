# frozen_string_literal:true

require 'minitest/autorun'
require 'minitest/pride'
require_relative '../lib/guess'
require_relative '../lib/card'

# Tests the CreditCheck Class
class GuessTest < Minitest::Test
  def setup
    @card = Card.new('Ace', 'Spades')
    @correct_guess = Guess.new('Ace of Spades', @card)
    @incorrect_guess = Guess.new('Grace of Spades', @card)
  end

  def test_it_instantiates
    assert_instance_of Guess, @correct_guess
  end

  def test_it_holds_a_card
    assert_instance_of Card, @correct_guess.card
  end

  def test_it_holds_a_response
    assert_equal 'Ace of Spades', @correct_guess.response
  end

  def test_it_can_be_correct
    assert @correct_guess.correct?
  end

  def test_it_can_be_incorrect
    refute @incorrect_guess.correct?
  end

  def test_it_gives_correct_feedback
    assert_equal 'Correct!', @correct_guess.feedback
  end

  def test_it_gives_incorrect_feedback
    assert_equal 'Incorrect!', @incorrect_guess.feedback
  end
end
