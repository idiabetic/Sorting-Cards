# frozen_string_literal:true

require 'minitest/autorun'
require 'minitest/pride'
require_relative '../lib/guess'
require_relative '../lib/card'
require_relative '../lib/deck'
require_relative '../lib/round'

# Tests the CreditCheck Class
class RoundTest < Minitest::Test
  def setup
    @card = Card.new('2', 'Spades')
    @card1 = Card.new('3', 'Spades')
    @card2 = Card.new('4', 'Spades')
    @deck = Deck.new([@card, @card1, @card2])
    @round = Round.new(@deck)
  end

  def test_it_instantiates
    assert_instance_of Round, @round
  end

  def test_it_holds_a_deck
    assert_instance_of Deck, @round.deck
  end

  def test_it_starts_without_guesses
    assert @round.guesses.empty?
  end

  def test_it_gets_the_current_card
    assert_equal @card, @round.current_card
  end

  def test_it_counts_guesses
    @round.record_guess(value: '4', suit: 'Spades')
    assert_equal 1, @round.guesses.count
  end

  def test_it_can_access_guess_attributes
    @round.record_guess(value: '2', suit: 'Spades')
    assert_equal 'Correct!', @round.guesses.first.feedback
  end

  def test_it_counts_correct_guesses
    @round.record_guess(value: '2', suit: 'Spades')
    assert_equal 1, @round.number_correct
  end

  def test_correct_guesses_remove_the_card
    @round.record_guess(value: '2', suit: 'Spades')
    refute @deck.card_array.include? @card
  end

  def test_it_keeps_score
    @round.record_guess(value: '2', suit: 'Spades')
    @round.record_guess(value: '7', suit: 'Spades')
    assert_equal '50.0%', @round.percent_correct
  end
end
